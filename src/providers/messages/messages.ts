import { Injectable } from '@angular/core';
import { StorageProvider } from '../storage/storage';

/*
  Generated class for the MessagesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessagesProvider {
  chats:any = [];newChats:any = [];

  constructor(private storageProvider: StorageProvider) {
  }
  getPrevMessages(){
    this.storageProvider.getStorageData('bots').then((res) => {
      this.chats = res;
    });
    this.storageProvider.getStorageData('user').then((res) => {
      this.chats.forEach((bot, i) => {
        this.storageProvider.getStorageData(bot.id + "::" + res.userId).then((mes_res) => {
          if (mes_res) {
            this.chats[i].lastMessage = mes_res[mes_res.length - 1].user+' : '+mes_res[mes_res.length - 1].message
            this.newChats.push(this.chats[i])
          }
        });
      });
    });
    return this.newChats;
  }
}
