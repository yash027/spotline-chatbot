import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage'; 
/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor( private storage: Storage) {
  }
  getStorageData(key){
    return this.storage.get(key);
  }
  setStorageData(key,data){
    this.storage.set(key,data)
  }
  logout() {
    this.storage.remove('user');
    this.storage.remove('bots');
    this.storage.remove('tenent');
  }
}
