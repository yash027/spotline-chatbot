import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
/*
  Generated class for the SynapseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SynapseProvider {

  private url = 'https://synapsenodeapp.azurewebsites.net';

  private socket;

  constructor(public http: HttpClient) {
    this.socket = io(this.url);
  }
  getHelpData(tenenturl) {
    return this.http.get(tenenturl + '/rest/help/getAllData');
  }
  login(data) {
    const formData: FormData = new FormData();
    formData.append('email', data.email);
    formData.append('password', data.password);
    return this.http.post(data.url + '/rest/common/auth/login', formData);
  }
  getTenent(acccountId) {
    return this.http.post(this.url + '/getTenent', { tenent: acccountId });
  }
  getAllBots(url) {
    return this.http.get(url + '/rest/common/getAllBotsForHomePage');
  }
  sendMessage(message) {
    message.id = this.socket.id;
    this.socket.emit('message', message);
    const observable = new Observable(observer => {
      this.socket.on('response', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }
    
}
