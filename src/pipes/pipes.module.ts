import { NgModule } from '@angular/core';
import { LinkifyPipe } from './linkify/linkify';
@NgModule({
	declarations: [LinkifyPipe],
	imports: [],
	exports: [LinkifyPipe]
})
export class PipesModule {}
