import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LinkifyPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'linkify',
})
export class LinkifyPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  // transform(value: string, ...args) {
  //   return value.toLowerCase();
  // }
  transform(value: any, args?: any): any {
    const urls = value.match(/(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)/g);
    if (urls) {
      urls.forEach(function (url) {
        // tslint:disable-next-line:max-line-length
        value = value.replace(url, '<a target="_blank" href="' + url + '">'+url+'</a>');
        // value = 'You are not a registered user. Please use the below link to get registered <br/>' + value;
      });
    }
    return value;
  }
}
