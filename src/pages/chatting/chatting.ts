import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, Content, TextInput } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-chatting',
  templateUrl: 'chatting.html'
})
export class ChattingPage {

  @ViewChild('chattingContent') content: Content;
  @ViewChild('messageInput') messageBox: TextInput

  public id: string; public bot: any = {}; public tenentUrl: string; public user: any = {};

  public allMessages: any = []; public message: string;
  con: Subscription;

  constructor(
    public navCtrl: NavController,
    private storageProvider: StorageProvider,
    public navParams: NavParams,
    public synapseProvider: SynapseProvider,
    public modalCtrl: ModalController) {
  }

  formatAMPM(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    const strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  uploadFile(files: FileList) {
    if (this.con) {
      this.con.unsubscribe();
    }
    let messageTime = new Date;
    const newMessage = { 'message': files.item(0), type: 'file', 'botId':  this.bot.id, 'brand': 'Smartdocs',
    'key': this.user.jwt, mimeType: files.item(0).type, name: files.item(0).name };
    this.allMessages.push({ user: 'me', message: files.item(0).name, type: 'PLAIN', at: messageTime });
    this.scrollToBottom();
    this.con = this.synapseProvider.sendMessage(newMessage).subscribe((res) => {
      messageTime = new Date;
      if (res != null) {
        if (res['type'] === 'PLAIN') {
          this.allMessages.push({ user: 'bot', message: res['message'], type: 'PLAIN', 'at': messageTime });
        } else {
          this.allMessages.push({ user: 'bot', 'message': res['message'], type: 'ADAPTIVECARD', 'at': messageTime });
        }
        this.scrollToBottom();
      }
    });
  }

  sendMessage() {
    this.messageBox.initFocus();
    if (this.con) {
      this.con.unsubscribe();
    }
    this.message = this.message.trim();
    if (this.message.length > 0) {
      let messageTime = new Date;
      this.allMessages.push({ user: 'me', message: this.message, type: 'PLAIN', at: messageTime });
      this.scrollToBottom();
      const newMessage = { 'message': this.message, 'botId': this.bot.id, 'brand': 'Smartdocs', 'key': this.user.jwt };
      this.con = this.synapseProvider.sendMessage(newMessage).subscribe((res) => {
        messageTime = new Date;
        if (res != null) {
          if (res['type'] === 'PLAIN') {
            this.allMessages.push({ user: 'bot', message: res['message'], type: 'PLAIN', 'at': messageTime });
          } else {
            this.allMessages.push({ user: 'bot', 'message': res['message'], type: 'ADAPTIVECARD', 'at': messageTime });
          }
          this.scrollToBottom();
        }
      });
      this.message = '';
    }
    else {
      this.message = '';
    }
  }

  ionViewWillEnter() {
    this.id = this.navParams.get('id');
    this.storageProvider.getStorageData('bots').then((res) => {
      this.bot = res.find(x => x.id === this.id)
    })

    this.storageProvider.getStorageData('user').then((res) => {
      this.user = res;
      const botMessages = this.id + '::' + res['userId']
      this.storageProvider.getStorageData(botMessages).then((res) => {
        if (res) {
          this.allMessages = res;
          this.scrollToBottom();
        } else {
          this.allMessages = [];
        }
      })
    })
    this.tenentUrl = this.navParams.get('tenentUrl');
  }

  ionViewDidEnter() {
    this.scrollToBottom();
  }

  ionViewDidLeave() {
    if (this.allMessages.length > 0) {
      this.storageProvider.setStorageData(this.id + '::' + this.user.userId, this.allMessages);
    }
  }

  ngOnDestoy() {
    if (this.allMessages.length > 0) {
      this.storageProvider.setStorageData(this.id + '::' + this.user.userId, this.allMessages);
    }
  }

  scrollToBottom() {
    if(!(this.content == null)){
      setTimeout( () => {
        this.content.scrollToBottom();
      }, 300);
    }
  }

  onKeyPress(keyCode) {
    if(keyCode === 13) {
      this.sendMessage();
    }
  }
}
