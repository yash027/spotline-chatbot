import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

 import { SigninPage } from '../signin/signin';
@Component({
  selector: 'page-myprofile',
  templateUrl: 'myprofile.html'
})
export class MyprofilePage {

  constructor(public navCtrl: NavController,private storageProvider: StorageProvider) {

  }
 
  signin(){
        this.navCtrl.setRoot(SigninPage)
  } 
  ngOnInit() {
    this.storageProvider.getStorageData('user').then((res) => {
    });
  }
  ionViewWillEnter(){
    this.storageProvider.getStorageData('user').then((res) => {
    });
  }
}
