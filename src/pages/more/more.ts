import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { StorageProvider } from '../../providers/storage/storage';
import { SigninPage } from '../signin/signin';
import { HelpPage } from '../help/help'
@Component({
      selector: 'page-more',
      templateUrl: 'more.html'
})
export class MorePage {

      public userData: any = {};

      constructor(public navCtrl: NavController,public appCtrl: App, private storageProvider: StorageProvider) {

      }

      privacy() {
            // this.navCtrl.push(PrivacyPage)
      }
      chatsetting() {
            // this.navCtrl.push(ChatsettingPage)
      }
      notificationsetting() {
            // this.navCtrl.push(NotificationsettingPage)
      }
      logout() {
            // this.navCtrl.popAll();
            this.storageProvider.logout();
            this.appCtrl.getRootNav().setRoot(SigninPage);

            // location.reload()
            // this.navCtrl.goToRoot(SigninPage)
      }
      help(){
            this.navCtrl.push(HelpPage);
      }
      ionViewWillEnter() {
            this.storageProvider.getStorageData('user').then((res) => {
                  this.userData = res;
            });
      }
}
