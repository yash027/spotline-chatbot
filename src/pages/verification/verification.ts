import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';

import { StorageProvider } from '../../providers/storage/storage';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { TabsPage } from '../tabs/tabs';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html'
})

export class VerificationPage {

  email: string=''; password: string=''; tenent: string; userLoginResponse: any = {};
  passwordType: any = 'password';

  constructor(
    public navCtrl: NavController,
    private synpaseService: SynapseProvider,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storageProvider: StorageProvider) {
  }

  login() {
    const loader = this.loadingController.create(
      {
        content: 'Please wait while you are getting logged in...'
      }
    );
    loader.present();
    const data = { url: this.tenent, email: this.email.toLowerCase(), password: this.password };
    this.synpaseService.login(data).subscribe((res) => {
      this.userLoginResponse = res;
      if (res['status'] === 'failed') {
        loader.dismiss();
        this.displayToast('Invalid Credentials.');
      }
      else {
        loader.dismiss();
        this.displayToast('You have successfully logged in.');
        this.storageProvider.setStorageData('user', res['result']);
        this.navCtrl.setRoot(TabsPage);
      }
    })
  }

  ngOnInit() {
    this.storageProvider.getStorageData('tenent').then((res) => {
      if (res != null) {
        this.tenent = res;
        this.storageProvider.getStorageData('user').then((resa) => {
          if (resa != null) {
            this.navCtrl.setRoot(TabsPage)
          }
        });        
      }
      else{
        this.navCtrl.setRoot(SigninPage);
      }
    });
  }

  onBack() {
    this.storageProvider.logout();
    this.navCtrl.setRoot(SigninPage);
  }

  displayToast(message) {
    let toast = this.toastController.create({
      message: message,
      duration: 1000,
      position: 'bottom'
    });
    toast.present();
  }
}
