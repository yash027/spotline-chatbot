import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
// import { Observable } from 'rxjs';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { StorageProvider } from '../../providers/storage/storage';
import { VerificationPage } from '../verification/verification';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {

  login: boolean = false;

  tenent: string = '';
  tenentData: any = {};

  constructor(
    public navCtrl: NavController,
    private synpaseService: SynapseProvider,
    private storageProvider: StorageProvider,
    private toastController: ToastController,
    private loadingController: LoadingController) {
  }

  verification() {
    this.tenent = this.tenent.trim();
    if (this.tenent.length > 0) {
      const loader = this.loadingController.create(
        {
          content: 'Please wait...'
        }
      );
      loader.present();
      this.synpaseService.getTenent(this.tenent.toLowerCase()).subscribe((res) => {
        this.tenentData = res;
        if (this.tenentData.status === 'success') {
          this.storageProvider.setStorageData('tenent', this.tenentData.url)
          this.navCtrl.setRoot(VerificationPage)
        } else {
          this.displayToast('Invalid Credentials');
        }
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.displayToast('Some Problem Occurred While Logging In, Please Try Later.');
      });
    } else {
      this.displayToast('Enter Account ID');
    }
  }

  ngOnInit() {
    this.storageProvider.getStorageData('tenent').then((res) => {
      if (res != null) {
        this.synpaseService.getAllBots(res).subscribe((resb) => {
          this.storageProvider.setStorageData('bots', resb['result'])
        })
        this.storageProvider.getStorageData('user').then((resa) => {
          if (resa === null) {
            this.navCtrl.setRoot(VerificationPage)
          }
          else {
            this.navCtrl.setRoot(TabsPage)
          }
        });
      }
    });
  }

  displayToast(message) {
    let toast = this.toastController.create({
      message: message,
      duration: 1000,
      position: 'bottom'
    });
    toast.present();
  }
}
