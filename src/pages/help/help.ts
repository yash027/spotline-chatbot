import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { StorageProvider } from '../../providers/storage/storage';
import { ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {
  @ViewChild(Content) content: Content;

  helpData:any = [];
  tenent:string = '';

  constructor(
    public navCtrl: NavController,
    private storageProvider: StorageProvider,
    public synapseProvider: SynapseProvider,
    public navParams: NavParams
    ) {
  }

  ionViewWillEnter() {
    this.storageProvider.getStorageData('tenent').then((res)=>{
      this.tenent  = res;
      this.synapseProvider.getHelpData(this.tenent).subscribe((data)=>{
        this.helpData = data['result'];
      })
    })

  }
  ionViewDidLoad(){
    let that = this;
    setTimeout(()=>{that.content.scrollToBottom();},200); 
  }

}
