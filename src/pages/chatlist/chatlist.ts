import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChattingPage } from '../chatting/chatting';
import { MessagesProvider } from '../../providers/messages/messages';
import { StorageProvider } from '../../providers/storage/storage';

@Component({
  selector: 'page-chatlist',
  templateUrl: 'chatlist.html'
})
export class ChatlistPage {
  bots: any = []; newChats: any = []; userId: string; tenentUrl: string;
  constructor(public navCtrl: NavController, public storageProvider: StorageProvider, public messagesProvider: MessagesProvider) {
  }

  chatting() {
    this.navCtrl.push(ChattingPage)
  }

  newchat() {
    // this.navCtrl.push(NewchatPage)
  }

  ngOnInt() {

  }

  startChat(id) {
    this.navCtrl.push(ChattingPage, { id: id, tenentUrl: this.tenentUrl })
  }

  ionViewWillEnter() {
    this.getData();
    // this.newChats = [];

  }
  filterMessages() {
    if (this.bots != null) {
      return this.bots.filter(x => x.lastMessage != null);
    }
  }
  getData() {
    this.bots = [];
    this.storageProvider.getStorageData('tenent').then((tenent) => {
      this.tenentUrl = tenent;
    });
    this.storageProvider.getStorageData('user').then((user) => {
      this.storageProvider.getStorageData('bots').then((avbots) => {
        this.bots = avbots;
        for (let i in this.bots) {
          this.storageProvider.getStorageData(this.bots[i].id + "::" + user.userId).then((mes_res) => {
            if (mes_res !== null) {
              if (mes_res[mes_res.length - 1].type == "PLAIN") {
                this.bots[i].lastMessage = mes_res[mes_res.length - 1].user + ' : ' + mes_res[mes_res.length - 1].message
              }
              else {
                this.bots[i].lastMessage = mes_res[mes_res.length - 1].user + ' : Attachment'
              }
            }
            else {
              this.bots[i].lastMessage = null;
            }
          });

        }
      });
    });
  }

  ionViewDidEnter() {
    this.getData();
  }
  
  ngOnInit() {

  }

  ionViewDidLeave() {
    this.bots = [];
  }
}
