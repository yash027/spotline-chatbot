import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { StorageProvider } from '../../providers/storage/storage';
import { ChattingPage } from '../chatting/chatting';
import { SynapseProvider } from '../../providers/synapse/synapse';

@Component({
  selector: 'page-grouplist',
  templateUrl: 'grouplist.html'
})
export class GrouplistPage {

  allBots: any = []; tenentUrl: string;

  constructor(public navCtrl: NavController, private synpaseService: SynapseProvider, private storageProvider: StorageProvider) {

  }

  startChat(id) {

    this.navCtrl.push(ChattingPage, { id: id, tenentUrl: this.tenentUrl })
  }
  creategroup() {
    // this.navCtrl.push(CreategroupPage)
  }
  getData() {
    if (this.allBots.length==0 ||this.allBots==null) {
      this.storageProvider.getStorageData('tenent').then((res) => {
        this.tenentUrl = res;
        this.synpaseService.getAllBots(res).subscribe((resb) => {
          this.storageProvider.setStorageData('bots', resb['result']);
          this.allBots =  resb['result'];
          this.getLastMessage();
        })
      });
    }
  }

  ngOnInit() {
  }

  getLastMessage() {
    if(this.allBots!=null){
      this.storageProvider.getStorageData('user').then((res) => {
        this.allBots.forEach((bot, i) => {
          this.storageProvider.getStorageData(bot.id + "::" + res.userId).then((mes_res) => {
            if (mes_res) {
              if (mes_res[mes_res.length - 1].type == "PLAIN") {
                this.allBots[i].lastMessage = mes_res[mes_res.length - 1].user + ' : ' + mes_res[mes_res.length - 1].message
              }
              else {
                this.allBots[i].lastMessage = mes_res[mes_res.length - 1].user + ' : Attachment'
              }
              // this.allBots[i].lastMessage = mes_res[mes_res.length - 1].user + ' : ' + mes_res[mes_res.length - 1].message
            }
            else {
              this.allBots[i].lastMessage = "Start Conversation..."
            }
          });
        });
      });
  
    }

  }
  ionViewWillEnter() {
    this.getData();
  }
  ionViewDidEnter() {
    // this.getData();
  }

  getShortDescription(description) {
    if(description) {
      return description.substring(0, 30) + '...';
    }
  }
}
