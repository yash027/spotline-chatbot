import { Component } from '@angular/core';

import { ChatlistPage } from '../chatlist/chatlist';
import { GrouplistPage } from '../grouplist/grouplist';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ChatlistPage;
  //tab3Root = ContactPage;
  tab3Root = GrouplistPage;
  tab4Root = MorePage;

  constructor() {

  }
  ionViewWillEnter(){
  }
}
