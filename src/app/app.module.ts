import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ChatlistPage } from '../pages/chatlist/chatlist';
import { ChattingPage } from '../pages/chatting/chatting';
import { GrouplistPage } from '../pages/grouplist/grouplist';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { MorePage } from '../pages/more/more';
import { ProfileinfoPage } from '../pages/profileinfo/profileinfo';
import { SigninPage } from '../pages/signin/signin';
import { TabsPage } from '../pages/tabs/tabs';
import { VerificationPage } from '../pages/verification/verification';
import { HelpPage } from '../pages/help/help';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SynapseProvider } from '../providers/synapse/synapse';
import { StorageProvider } from '../providers/storage/storage';
import { IonicStorageModule } from '@ionic/storage';
import { MessagesProvider } from '../providers/messages/messages';

import { LinkifyPipe } from '../pipes/linkify/linkify';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,  
    ChatlistPage,
    ChattingPage,
    GrouplistPage,  
    MyprofilePage,
    MorePage,
    ProfileinfoPage,
    SigninPage,
    HelpPage,
    LinkifyPipe,
    TabsPage,
    VerificationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,   
    ChatlistPage,
    ChattingPage,
    GrouplistPage,   
    MyprofilePage,
    MorePage,
    ProfileinfoPage,
    SigninPage,
    HelpPage,
    TabsPage,
    VerificationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SynapseProvider,
    StorageProvider,
    MessagesProvider
  ]
})
export class AppModule {}
