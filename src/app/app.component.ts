import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { SigninPage } from '../pages/signin/signin';
import { Storage } from '@ionic/storage';
import { TranslateService } from '../../node_modules/@ngx-translate/core';
import { VerificationPage } from '../pages/verification/verification';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SigninPage;

  constructor(private platform: Platform, private statusBar: StatusBar,
    private storageProvider:Storage,
  private splashScreen: SplashScreen, public translate:TranslateService) {
    this.initializeApp();
  }
  
  initializeApp() {
    this.platform.ready().then(() => {
      this.storageProvider.get('tenent').then((tenent)=>{
        if(tenent!==null){
          this.storageProvider.get('user').then((user)=>{
            if(user!==null){
              this.rootPage = TabsPage;
            }
            else{
              this.rootPage = VerificationPage;
            }
          })
        }
        else{
          this.rootPage = SigninPage;
        }
      })
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.translate.setDefaultLang('en');
      this.translate.use('en');
    });
  }
}
